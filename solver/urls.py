from django.conf.urls import url
from solver import views,solver9,solver4,solver16

urlpatterns = [
    url(r'^$', solver9.index),
    url(r'^sudoku_4/$', solver4.a4),
    url(r'^sudoku_16/$', solver16.sud16),
    url(r'^jawaban/$', solver4.jawaban),
    url(r'^answer/$', solver9.answer),
    url(r'^hasil/$', solver16.hasil),
]
