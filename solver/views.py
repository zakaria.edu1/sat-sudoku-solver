from django.shortcuts import render
from django.views.generic import TemplateView

# Create your views here.
class sudoku_9(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'sudoku_9.html', context=None)

class sudoku_4(TemplateView):
    template_name = "sudoku_4.html"

class sudoku_16(TemplateView):
    template_name = "sudoku_16.html"

class solver_4(TemplateView):
    template_name = "solver_4.html"

class solver_9(TemplateView):
    template_name = "solver_9.html"

class solver_16(TemplateView):
    template_name = "solver_16.html"
